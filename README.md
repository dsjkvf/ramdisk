## RAMDisk ##

This is a simple Python script that

  - on OS X creates a RAM disk (size is adjustable)
  - may map different cache directories to this RAM disk (see options inside the script)

Should be launched[^1] on start or wake up, plays nicely with [SleepWatcher](https://bitbucket.org/dsjkvf/ramdisk/) daemon.


[^1]: with a command like this: `if [ ! -d "/Volumes/RAMDisk" ] ; then rmd.py ; fi`
