#!/usr/bin/env python


# LEGAL

__author__ = "dsjkvf"
__credits__ = []
__license__ = "Revised BSD"
__maintainer__ = "dsjkvf"
__email__ = "dsjkvf@gmail.com"
__status__ = "Production"
__version__ = "1.0.5"
__url__ = "https://bitbucket.org/dsjkvf/ramdisk/"


# IMPORT MODULES

import sys
import os
import re
import shutil


# DEFINE GLOBAL VARIABLES

# Set size of the RAM disk
sizeMBytes = 500

# List the directories to re-map
chromium = ('~/Library/Caches/Chromium/Default/Cache',
            '~/Library/Caches/Chromium/Default/Media Cache',
            '~/Library/Caches/Chromium/PnaclTranslationCache',
            '~/Library/Application Support/Chromium/Default/Extension State')
google_chrome = ('~/Library/Caches/Google/Chrome',)
firefox = ('~/Library/Caches/Firefox',)

# Choose (uncomment) the set you need
# dirs = chromium
# dirs = google_chrome
# dirs = firefox

# Or use them all
# dirs = chromium + google_chrome + firefox

# Or add your own
# dirs = 


# DEFINE HELPER FUNCTIONS

# Create the RAM disk 
def create():
    # convert the desired size of the RAM disk
	sizeBytes = str(int(sizeMBytes) * 1953)
	# create the RAM disk itself
	os.system('diskutil erasevolume HFS+ \"RAMDisk\" `hdiutil attach -nomount ram://'+sizeBytes+'`')

# According to the given set of directories create a set of the directories to be reproduced in the RAM disk
def relist(d):
    for i in range(len(d)):
    	d[i] = re.sub('~/Library','/Volumes/RAMDisk',d[i])

# Create the requested filesystem
def create_fs():
	dirsRAM = list(dirs)
	relist(dirsRAM)
	# create directories in the RAM disk
	for i in dirsRAM:
		try:
			os.makedirs(os.path.expanduser(i))
		except OSError, e:
			pass
	# symlink directories in the RAM disk to corresponding mountpoints in the HDD/SSD
	for (i,j) in zip(dirs,dirsRAM):
		if os.path.exists(os.path.expanduser(i)):
			if not os.path.islink(os.path.expanduser(i)):
				shutil.rmtree(os.path.expanduser(i))
				os.symlink(os.path.expanduser(j), os.path.expanduser(i))
		else:
			try:
				os.symlink(os.path.expanduser(j), os.path.expanduser(i))
			except OSError, e:
					os.unlink(os.path.expanduser(i))
					os.symlink(os.path.expanduser(j), os.path.expanduser(i))
	# hide the RAM disk
	os.system('chflags hidden /Volumes/RAMDisk')


# CREATE WORKSPACE

def main():
	if not len(sys.argv) > 1:
		create()
		try:
		    dirs
		except NameError:
		    print ('The RAM Disk has been successfully created. However, no additional file system was requested.')
		else:
		    create_fs()
		sys.exit()
	else:
		print ('ATTENTION: no command line parameters are supposed to be used with this script')
		sys.exit()


# START EVERYTHING

main()
